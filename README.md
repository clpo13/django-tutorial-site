# django tutorial

Django/Python tutorial site. See
<https://docs.djangoproject.com/en/2.2/intro/tutorial01/>.

Run a development server with `python manage.py runserver` and access it at
<http://localhost:8000>.

To create a user for the admin interface, run `python manage.py createsuperuser`.
The interface can be accessed at <http://localhost:8000/admin>.
